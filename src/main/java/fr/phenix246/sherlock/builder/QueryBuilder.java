package fr.phenix246.sherlock.builder;

import fr.phenix246.sherlock.builder.argument.ArgumentParser;
import fr.phenix246.sherlock.builder.operation.PredicateBuilder;
import fr.phenix246.sherlock.builder.property.Property;
import fr.phenix246.sherlock.builder.property.PropertyMapper;
import fr.phenix246.sherlock.parser.ast.*;

import javax.persistence.criteria.*;
import java.util.List;

public class QueryBuilder<T> implements SherlockVisitor<Predicate, SherlockBuilderParameter> {

    private final CriteriaBuilder criteriaBuilder;
    private final PropertyMapper propertyMapper;
    private final ArgumentParser argumentParser;
    private final Root<T> root;

    public QueryBuilder(CriteriaBuilder criteriaBuilder, PropertyMapper propertyMapper, ArgumentParser argumentParser, Root<T> root) {
        this.criteriaBuilder = criteriaBuilder;
        this.propertyMapper = propertyMapper;
        this.argumentParser = argumentParser;
        this.root = root;

    }

    @Override
    public Predicate visit(AndNode node, SherlockBuilderParameter param) {
        return criteriaBuilder.and(visitChildren(node.getChildren(), param));
    }

    @Override
    public Predicate visit(OrNode node, SherlockBuilderParameter param) {
        return criteriaBuilder.or(visitChildren(node.getChildren(), param));
    }

    @Override
    public Predicate visit(ComparisonNode node, SherlockBuilderParameter param) {
        Predicate[] comparisons = new Predicate[node.getSelectors().size()];

        int index = 0;
        for(String selector : node.getSelectors()) {
            Property property = propertyMapper.map(selector, root.getModel().getJavaType());
            Class<?> propertyType = property.getType(root.getModel());

            comparisons[index++] = PredicateBuilder.createPredicate(
                    property.getPath(root),
                    node.getOperator(),
                    argumentParser.parse(node.getArguments(), propertyType),
                    param,
                    criteriaBuilder);
        }

        switch (node.getSelectorsOperator()) {
            case AND: return criteriaBuilder.and(comparisons);
            case OR: return criteriaBuilder.or(comparisons);
            default: throw new IllegalArgumentException("Illegal selector operator");
        }
    }

    private Predicate[] visitChildren(List<Node> children, SherlockBuilderParameter param) {
        Predicate[] childrenPredicate = new Predicate[children.size()];

        int index = 0;
        for (Node child : children) {
            if (child instanceof AndNode)
                childrenPredicate[index++] = this.visit((AndNode) child, param);
            else if (child instanceof OrNode)
                childrenPredicate[index++] = this.visit((OrNode) child, param);
            else
                childrenPredicate[index++] = this.visit((ComparisonNode) child, param);
        }

        return childrenPredicate;
    }
}
