package fr.phenix246.sherlock.builder.argument;

import java.util.List;

public interface ArgumentParser {

    <T> T parse(String argument, Class<T> type) throws ArgumentFormatException, IllegalArgumentException;

    <T> List<Object> parse(List<String> arguments, Class<T> type)
            throws ArgumentFormatException, IllegalArgumentException;
}
