package fr.phenix246.sherlock.builder.argument;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SimpleArgumentParser implements ArgumentParser {

    private static final String DATE_PATTERN = "yyyy-MM-dd"; //ISO 8601
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"; //ISO 8601

    @Override
    public <T> T parse(String argument, Class<T> type) throws ArgumentFormatException, IllegalArgumentException {

        if (argument == null || "null".equals(argument.trim().toLowerCase())) {
            return (T) null;
        }

        try {
            // String
            if (type.equals(String.class)) return (T) argument;
            if (type.equals(Character.class) || type.equals(char.class)) return (T) Short.valueOf(argument);
            // Boolean
            if (type.equals(Boolean.class) || type.equals(boolean.class)) return (T) Boolean.valueOf(argument);
            // Number
            if (type.equals(Byte.class) || type.equals(byte.class)) return (T) Short.valueOf(argument);
            if (type.equals(Short.class) || type.equals(short.class)) return (T) Short.valueOf(argument);
            if (type.equals(Integer.class) || type.equals(int.class)) return (T) Integer.valueOf(argument);
            if (type.equals(Long.class) || type.equals(long.class)) return (T) Long.valueOf(argument);
            if (type.equals(Float.class) || type.equals(float.class)) return (T) Float.valueOf(argument);
            if (type.equals(Double.class) || type.equals(double.class)) return (T) Double.valueOf(argument);
            if (type.equals(BigDecimal.class)) return (T) new BigDecimal(argument);
            if (type.equals(BigInteger.class)) return (T) new BigInteger(argument);
            // Enum
            if (type.isEnum()) return (T) Enum.valueOf((Class<Enum>) type, argument);
            // UUID
            if (type.equals(UUID.class)) return (T) UUID.fromString(argument);
            // Date
            if (type.equals(Date.class)) return (T) parseDate(argument, type);
            if (type.equals(LocalDate.class)) return (T) parseLocalDate(argument, type);
            if (type.equals(LocalTime.class)) return (T) parseLocalTime(argument, type);
            if (type.equals(LocalDateTime.class)) return (T) parseLocalDateTime(argument, type);
            if (type.equals(Instant.class)) return (T) parseInstant(argument, type);
        } catch (IllegalArgumentException ex) {
            throw new ArgumentFormatException(argument, type);
        }
        return null;
    }

    @Override
    public <T> List<Object> parse(List<String> arguments, Class<T> type) throws ArgumentFormatException, IllegalArgumentException {
        List<Object> castedArguments = new ArrayList<>(arguments.size());
        for (String argument : arguments) {
            castedArguments.add(this.parse(argument, type));
        }
        return castedArguments;
    }

    private <T> Date parseDate(String argument, Class<T> type) {
        try {
            return new SimpleDateFormat(DATE_TIME_PATTERN).parse(argument);
        } catch (ParseException e) {
            // try the other format
        }

        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(argument);
        } catch (ParseException ex1) {
            throw new ArgumentFormatException(argument, type);
        }
    }

    private <T> LocalDate parseLocalDate(String argument, Class<T> type) {
        try {
            return LocalDate.parse(argument);
        } catch (DateTimeParseException e) {
            // try the other format
        }

        try {
            return LocalDate.parse(argument, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } catch (DateTimeParseException e) {
            throw new ArgumentFormatException(argument, type);
        }
    }

    private <T> LocalTime parseLocalTime(String argument, Class<T> type) {
        try {
            return LocalTime.parse(argument);
        } catch (DateTimeParseException e) {
            // try the other format
        }

        try {
            return LocalTime.parse(argument, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } catch (DateTimeParseException e) {
            throw new ArgumentFormatException(argument, type);
        }
    }

    private <T> LocalDateTime parseLocalDateTime(String argument, Class<T> type) {
        try {
            return LocalDateTime.parse(argument);
        } catch (DateTimeParseException e) {
            // try the other format
        }

        try {
            return LocalDateTime.parse(argument, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            // try the other format
        }

        try {
            return LocalDateTime.parse(argument, DateTimeFormatter.ISO_LOCAL_TIME);
        } catch (DateTimeParseException e) {
            throw new ArgumentFormatException(argument, type);
        }
    }

    private <T> Instant parseInstant(String argument, Class<T> type) {
        try {
            return Instant.parse(argument);
        } catch (DateTimeParseException e) {
            throw new ArgumentFormatException(argument, type);
        }
    }
}
