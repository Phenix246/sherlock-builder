package fr.phenix246.sherlock.builder.operation;

import fr.phenix246.sherlock.builder.SherlockBuilderParameter;
import fr.phenix246.sherlock.parser.ast.ComparisonOperator;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

public class PredicateBuilder {

    private static final Character LIKE_WILDCARD = '*';

    public static Predicate createPredicate(Expression property, ComparisonOperator operator, List<Object> arguments, SherlockBuilderParameter parameter, CriteriaBuilder cb) {
        var op = SherlockOperatorProxy.asEnum(operator);
        if (op == null)
            throw new IllegalArgumentException("Unknown operator: " + operator);
        switch (op) {
            case EQUAL: {
                Object argument = arguments.get(0);
                if (argument == null) {
                    return createIsNull(property, cb);
                } else {
                    return createEqual(property, argument, cb);
                }
            }
            case NOT_EQUAL: {
                Object argument = arguments.get(0);
                if (argument == null) {
                    return createIsNotNull(property, cb);
                } else {
                    return createNotEqual(property, argument, cb);
                }
            }
            case GREATER_THAN: {
                Object argument = arguments.get(0);
                if (argument instanceof Number || argument == null) {
                    return createGreaterThan(property, (Number) argument, cb);
                } else if (argument instanceof Comparable) {
                    return createGreaterThan(property, (Comparable) argument, cb);
                } else {
                    throw new IllegalArgumentException();
                }
            }
            case GREATER_THAN_OR_EQUAL: {
                Object argument = arguments.get(0);
                if (argument instanceof Number || argument == null) {
                    return createGreaterEqual(property, (Number) argument, cb);
                } else if (argument instanceof Comparable) {
                    return createGreaterEqual(property, (Comparable) argument, cb);
                } else {
                    throw new IllegalArgumentException();
                }
            }
            case LESS_THAN: {
                Object argument = arguments.get(0);
                if (argument instanceof Number || argument == null) {
                    return createLessThan(property, (Number) argument, cb);
                } else if (argument instanceof Comparable) {
                    return createLessThan(property, (Comparable) argument, cb);
                } else {
                    throw new IllegalArgumentException();
                }
            }
            case LESS_THAN_OR_EQUAL: {
                Object argument = arguments.get(0);
                if (argument instanceof Number || argument == null) {
                    return createLessEqual(property, (Number) argument, cb);
                } else if (argument instanceof Comparable) {
                    return createLessEqual(property, (Comparable) argument, cb);
                } else {
                    throw new IllegalArgumentException();
                }
            }
            case IN: {
                return createIn(property, arguments, cb);
            }
            case NOT_IN: {
                return createNotIn(property, arguments, cb);
            }
            case LIKE: {
                String propertyType = property.getJavaType().getSimpleName();
                String argument = (String) arguments.get(0);
                if ("String".equals(propertyType)) {
                    return createLike(property, argument, cb);
                } else {
                    return createLike(convertDate(property, propertyType, parameter, cb), argument, cb);
                }
            }
            case NOT_LIKE: {
                String propertyType = property.getJavaType().getSimpleName();
                String argument = (String) arguments.get(0);
                if ("String".equals(propertyType)) {
                    return createNotLike(property, argument, cb);
                } else {
                    return createNotLike(convertDate(property, propertyType, parameter, cb), argument, cb);
                }
            }
        }
        throw new IllegalArgumentException("Unknown operator: " + operator);
    }

    private static Expression<String> convertDate(Expression<?> property, String propertyType, SherlockBuilderParameter parameter, CriteriaBuilder cb) {
        switch (propertyType) {
            case "Date":
            case "Instant":
            case "LocalDate":
                return buildOperationFromPattern(property, parameter.getDateFormat(), cb);
            case "LocalTime":
                return buildOperationFromPattern(property, parameter.getTimeFormat(), cb);
            case "LocalDateTime":
                return buildOperationFromPattern(property, parameter.getDateTimeFormat(), cb);
            default:
                return (Expression<String>) property;
        }
    }

    private static Predicate createEqual(Expression<?> property, Object argument, CriteriaBuilder cb) {
        return cb.equal(property, argument);
    }

    private static Predicate createEqual(Expression<?> left, Expression<?> right, CriteriaBuilder cb) {
        return cb.equal(left, right);
    }

    private static Predicate createNotEqual(Expression<?> property, Object argument, CriteriaBuilder cb) {
        return cb.notEqual(property, argument);
    }

    private static Predicate createNotEqual(Expression<?> left, Expression<?> right, CriteriaBuilder cb) {
        return cb.notEqual(left, right);
    }

    private static Predicate createLike(Expression<String> property, String argument, CriteriaBuilder cb) {
        String like = argument.replace(LIKE_WILDCARD, '%');
        return cb.like(cb.lower(property), like.toLowerCase());
    }

    private static Predicate createNotLike(Expression<String> property, String argument, CriteriaBuilder cb) {
        String like = argument.replace(LIKE_WILDCARD, '%');
        return cb.notLike(cb.lower(property), like.toLowerCase());
    }

    private static Expression<String> buildOperationFromPattern(Expression<?> property, String pattern, CriteriaBuilder cb) {
        return cb.function("TO_CHAR", String.class, property, cb.literal(pattern));
    }

    private static Predicate createBetweenThan(Expression<Date> property, Date start, Date end, CriteriaBuilder cb) {
        return cb.between(property, start, end);
    }

    private static Predicate createIsNull(Expression<?> property, CriteriaBuilder cb) {
        return cb.isNull(property);
    }

    private static Predicate createIsNotNull(Expression<?> property, CriteriaBuilder cb) {
        return cb.isNotNull(property);
    }

    private static Predicate createGreaterThan(Expression<? extends Number> property, Number argument, CriteriaBuilder cb) {
        return cb.gt(property, argument);
    }

    private static <Y extends Comparable<? super Y>> Predicate createGreaterThan(Expression<? extends Y> property, Y argument, CriteriaBuilder cb) {
        return cb.greaterThan(property, argument);
    }

    private static Predicate createGreaterEqual(Expression<? extends Number> property, Number argument, CriteriaBuilder cb) {
        return cb.ge(property, argument);
    }

    private static <Y extends Comparable<? super Y>> Predicate createGreaterEqual(Expression<? extends Y> property, Y argument, CriteriaBuilder cb) {
        return cb.greaterThanOrEqualTo(property, argument);
    }

    private static Predicate createLessThan(Expression<? extends Number> property, Number argument, CriteriaBuilder cb) {
        return cb.lt(property, argument);
    }

    private static <Y extends Comparable<? super Y>> Predicate createLessThan(Expression<? extends Y> property, Y argument, CriteriaBuilder cb) {
        return cb.lessThan(property, argument);
    }

    private static Predicate createLessEqual(Expression<? extends Number> property, Number argument, CriteriaBuilder cb) {
        return cb.le(property, argument);
    }

    private static <Y extends Comparable<? super Y>> Predicate createLessEqual(Expression<? extends Y> property, Y argument, CriteriaBuilder cb) {
        return cb.lessThanOrEqualTo(property, argument);
    }

    private static Predicate createIn(Expression<?> propertyPath, List<?> arguments, CriteriaBuilder cb) {
        return propertyPath.in(arguments);
    }

    private static Predicate createNotIn(Expression<?> propertyPath, List<?> arguments, CriteriaBuilder cb) {
        return cb.not(propertyPath.in(arguments));
    }
}
