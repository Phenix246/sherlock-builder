package fr.phenix246.sherlock.builder.operation;

import fr.phenix246.sherlock.parser.ast.ComparisonOperator;
import fr.phenix246.sherlock.parser.ast.SherlockOperators;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum SherlockOperatorProxy {

    EQUAL(SherlockOperators.EQUAL),
    NOT_EQUAL(SherlockOperators.NOT_EQUAL),
    GREATER_THAN(SherlockOperators.GREATER_THAN),
    GREATER_THAN_OR_EQUAL(SherlockOperators.GREATER_THAN_OR_EQUAL),
    LESS_THAN(SherlockOperators.LESS_THAN),
    LESS_THAN_OR_EQUAL(SherlockOperators.LESS_THAN_OR_EQUAL),
    LIKE(SherlockOperators.LIKE),
    NOT_LIKE(SherlockOperators.NOT_LIKE),
    IN(SherlockOperators.IN),
    NOT_IN(SherlockOperators.NOT_IN);

    private final ComparisonOperator operator;

    private final static Map<ComparisonOperator, SherlockOperatorProxy> CACHE =
            Collections.synchronizedMap(new HashMap<>());

    static {
        for (SherlockOperatorProxy proxy : values()) {
            CACHE.put(proxy.getOperator(), proxy);
        }
    }

    SherlockOperatorProxy(ComparisonOperator operator) {
        this.operator = operator;
    }

    public ComparisonOperator getOperator() {
        return operator;
    }

    public static SherlockOperatorProxy asEnum(ComparisonOperator operator) {
        return CACHE.get(operator);
    }
}
