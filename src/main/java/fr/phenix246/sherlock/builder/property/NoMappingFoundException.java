package fr.phenix246.sherlock.builder.property;

public class NoMappingFoundException extends RuntimeException {

    private final Class<?> propertyType;

    public NoMappingFoundException(Class<?> propertyType) {
        super("Cannot find a mapping for class " + propertyType);
        this.propertyType = propertyType;
    }

    public Class<?> getPropertyType() {
        return propertyType;
    }
}
