package fr.phenix246.sherlock.builder.property;

import fr.phenix246.sherlock.builder.util.Util;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

public class Property {

    private String property;

    public Property(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public void addPrefix(String prefix) {
        this.property = prefix + "." + property;
    }

    public Class<?> getType(EntityType<?> rootEntityType) {
        return Util.getType(rootEntityType, this.property);
    }

    /**
     * Get the path for the attribut
     * @param root The root
     * @param <Y>
     * @param <X>
     * @return The path
     */
    public <X,Y> Path<Y> getPath(Root<X> root) {
        return Util.getPath(root, this.property);
    }
}
