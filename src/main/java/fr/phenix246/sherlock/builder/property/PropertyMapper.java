package fr.phenix246.sherlock.builder.property;

public interface PropertyMapper {

    default <T> Property map(String selector, Class<T> type) throws NoMappingFoundException {
        return map(selector.split("\\."), type, 0);
    }

    <T> Property map(String[] selectors, Class<T> type, int index) throws NoMappingFoundException;
}
