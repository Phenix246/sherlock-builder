package fr.phenix246.sherlock.builder.property;

import org.javatuples.Pair;

import java.util.Map;

public class SimplePropertyMapper implements PropertyMapper {

    Map<Class<?>, Map<String, Pair<String, Class<?>>>> mapping;

    @Override
    public <T> Property map(String selector, Class<T> type) throws NoMappingFoundException {
        if (mapping.isEmpty()) return new Property(selector);

        return map(selector.split("\\."), type, 0);
    }

    @Override
    public <T> Property map(String[] selectors, Class<T> type, int index) throws NoMappingFoundException {
        Map<String, Pair<String, Class<?>>> classMapping = mapping.get(type);

        if (classMapping == null) {
            throw new NoMappingFoundException(type);
        }
        Pair<String, Class<?>> pair = classMapping.get(selectors[index]);
        if (pair == null) {
            throw new NoMappingFoundException(type);
        }

        if (++index < selectors.length) {
            Property property = map(selectors, pair.getValue1(), index);
            property.addPrefix(pair.getValue0());
            return property;
        } else {
            return new Property(pair.getValue0());
        }
    }

    public <T> void addMapping(Class<T> type, Map<String, Pair<String, Class<?>>> mapping) {
        this.mapping.put(type, mapping);
    }

    public void setMapping(Map<Class<?>, Map<String, Pair<String, Class<?>>>> mapping) {
        this.mapping = mapping;
    }
}
