package fr.phenix246.sherlock.builder.util;

import org.hibernate.jpa.criteria.path.PluralAttributePath;

import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;

public class Util {

    /**
     * Return the Data Type
     * @param attribute The SingularAttribute
     * @return The Type
     */
    public static Class<?> getType(SingularAttribute<?,?> attribute) {
        return attribute.getJavaType();
    }

    /**
     * Return the Data Type
     * @param type The root model
     * @param field The field
     * @return The Type
     */
    public static Class<?> getType(EntityType<?> type, String field) {
        SingularAttribute<?,?> att = null;

        for (final String part : field.split("\\.")) {
            SingularAttribute<?,?>  tmp = type.getSingularAttribute(part);
            if (att == null) {
                att = tmp;
            }
            if (tmp.getType() instanceof EntityType) {
                type = (EntityType<?>) tmp.getType();
            } else {
                break;
            }
        }

        assert att != null;
        return getType(att);
    }

    public static <Y> Path<Y> getPath(@SuppressWarnings("rawtypes") Root root, String property) {
        @SuppressWarnings("unchecked")
        Path<Y> path = root;
        @SuppressWarnings("unchecked")
        From<Root<?>, Y> from = root;
        for (final String part : property.split("\\.")) {
            path = path.get(part);

            if (path instanceof PluralAttributePath) {
                from = from.join(part, JoinType.LEFT);
                path = from;
            }
        }
        return path;
    }
}
